package com.example.jonatan.ejemplorecyclerview;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import com.example.jonatan.ejemplorecyclerview.Entity.Usuario;

/**
 * Created by Jonatan on 30/06/2018.
 */

public class NuevoUsuarioActivity extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle persistenceBundle) {
        super.onCreate(persistenceBundle);
        setContentView(R.layout.activity_add_user);

        findViewById(R.id.btnConfirmNewUser).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText etEmail = (EditText) findViewById(R.id.etEmail);
                EditText etPassword = (EditText) findViewById(R.id.etPassword);

                String email = etEmail.getText().toString();
                String password = etPassword.getText().toString();

                Usuario usuario = new Usuario(email, password);

                Intent i = new Intent();
                i.putExtra("usuarioAgregado", usuario);
                setResult(RESULT_OK, i);
                finish();
            }
        });
    }
}
