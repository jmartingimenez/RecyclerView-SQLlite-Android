package com.example.jonatan.ejemplorecyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.jonatan.ejemplorecyclerview.Entity.Usuario;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Jonatan on 30/06/2018.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>{
    private List<Usuario> usuarios = new LinkedList<Usuario>();
    private TextView textView;

    /*Cada vez que seteo el Adapter recibo la lista de users de la BDD actualizada.
    * Como puse en el 'MainActivioty', no estoy seguro si esta bien hacer esto */
    public RecyclerViewAdapter(List<Usuario> usuarios){
        /*El adaptador tiene una referencia a la lista, así que no debe asignarsele
        * una lista distinta, sino vaciarla y agregarle la lista que llega*/
        this.usuarios.clear();
        this.usuarios.addAll(usuarios);
    }

    @Override
    /*Encargado de crear los nuevos objetos ViewHolder necesarios para los elementos
    de la colección. Aca se recrea la vista para un item*/
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.contenedor_texto_recyclerview,
                parent, false);
        return new ViewHolder(v);
    }

    @Override
    /*Encargado de actualizar los datos de un ViewHolder ya existente.
    * el 'position' tiene la posición del adaptador*/
    public void onBindViewHolder(RecyclerViewAdapter.ViewHolder holder, int position) {
        if(!usuarios.isEmpty()){
            textView.setText("Email: " + usuarios.get(position).getEmail() +
                    ". Pass: " + usuarios.get(position).getPassword());
        }
    }

    @Override
    /*Indica el número de elementos de la colección de datos.*/
    public int getItemCount() {
        return usuarios.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.textoRV);
        }
    }

    /*Agregados para evitar error al scrollear mucho*/
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
