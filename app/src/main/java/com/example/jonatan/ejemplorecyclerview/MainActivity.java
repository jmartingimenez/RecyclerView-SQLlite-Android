package com.example.jonatan.ejemplorecyclerview;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import com.example.jonatan.ejemplorecyclerview.Dao.UsuarioDao;
import com.example.jonatan.ejemplorecyclerview.Util.UsuarioSQLHelper;
import com.example.jonatan.ejemplorecyclerview.Entity.Usuario;

public class MainActivity extends AppCompatActivity {
    private final int REQUEST_CODE = 999;
    private UsuarioDao usuarioDao;
    private RecyclerView rv;

    @Override
    /*Estoy seteando el Adapter cada vez que esta actividad recibe el resultado. No
    * estoy seguro de que sea lo correcto, pero no sabía como pasarle la lista a la
    * clase. (Le cree un constructor que recibe esta lista y la asigna a una propia)*/
    protected void onActivityResult(int requestCode, int result_code, Intent data){
        super.onActivityResult(requestCode, result_code, data);
        if(requestCode == REQUEST_CODE && result_code == RESULT_OK){
            usuarioDao.add((Usuario)data.getParcelableExtra("usuarioAgregado"));
            rv.setAdapter(new RecyclerViewAdapter(usuarioDao.findAll()));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rv = (RecyclerView) findViewById(R.id.recyclerView);
        rv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        /*GridLayoutManager (el 2 es la cantidad de columnas)
        rv.setLayoutManager(new GridLayoutManager(this, 2));
        */

        findViewById(R.id.btnAddNote).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, NuevoUsuarioActivity.class);
                startActivityForResult(i, REQUEST_CODE);
            }
        });

        /*Inicilializo el objeto para acceder a la BDD. Además, seteo el adapter con
        * la los datos existentes (Si se abre la aplicación y había datos guardados
        * van a aparecer)*/
        usuarioDao = new UsuarioDao(new UsuarioSQLHelper(getApplicationContext(),
                "UsuariosEjemploRecyclerView", null, 1));
        rv.setAdapter(new RecyclerViewAdapter(usuarioDao.findAll()));
    }
}
