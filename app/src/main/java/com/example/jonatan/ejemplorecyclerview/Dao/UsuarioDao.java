package com.example.jonatan.ejemplorecyclerview.Dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.jonatan.ejemplorecyclerview.Entity.Usuario;
import com.example.jonatan.ejemplorecyclerview.Util.UsuarioSQLHelper;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Jonatan on 30/06/2018.
 */

/*Haciendo a mano hasta revisar el ORM. Revisar luego sintaxis OOP para evitar
* inyecciones*/
public class UsuarioDao {
    private SQLiteDatabase db;

    public UsuarioDao(UsuarioSQLHelper dbHelper){
        db = dbHelper.getWritableDatabase();
    }

    public Usuario add(Usuario usuario){
        db.execSQL( "INSERT INTO Usuario (email, password) " +
                    "VALUES('"+usuario.getEmail()+"', '"+usuario.getPassword()+"')");
        return usuario;
    }

    public List<Usuario> findAll(){
        Cursor c = db.rawQuery("SELECT * FROM Usuario", null);
        List<Usuario> usuarios = new LinkedList<Usuario>();

        //Si hay al menos un registro...
        if(c.moveToFirst()){
            do{
                String email = c.getString(0);
                String password = c.getString(1);
                usuarios.add(new Usuario(email, password));
            }while(c.moveToNext());
        }
        return usuarios;
    }
}
