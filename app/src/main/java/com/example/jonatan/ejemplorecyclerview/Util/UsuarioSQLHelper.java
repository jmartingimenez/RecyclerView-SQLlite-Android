package com.example.jonatan.ejemplorecyclerview.Util;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


/**
 * Created by Jonatan on 30/06/2018.
 */

public class UsuarioSQLHelper extends SQLiteOpenHelper {
    private final String sqlCreate = "CREATE TABLE Usuario(email TEXT, password TEXT)";

    public UsuarioSQLHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    /*Se ejecuta automaticamente cuando sea necesario crear la BDD o hacer inserciones
    * iniciales*/
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreate);
    }

    @Override
    /*Cuando sea necesario actualizar la estructura de la BDD. Ejemplo: Agregar un
    * campo*/
    public void onUpgrade(SQLiteDatabase db, int versionAnterior, int versionNueva) {
        //Mala practica. Por el momento lo dejo así (hay que migrar de una db a otra)
        db.execSQL("DROP TABLE IF EXISTS Usuario");
        db.execSQL(sqlCreate);
    }
}
